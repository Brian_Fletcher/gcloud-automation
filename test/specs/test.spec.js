const { Builder } = require('selenium-webdriver');
const { describe, it } = require('mocha');
const IndexPage = require('../page/index.page');
const SearchPage = require('../page/search.page');
const CalculatorPage = require('../page/calculator.page');

describe("Google Cloud", function() {
    this.timeout(30000);

    let driver;
    let indexPage;
    let searchPage;
    let calculatorPage;

    before(async function() {
        driver = await new Builder().forBrowser('chrome').build();
        indexPage = new IndexPage(driver);
        searchPage = new SearchPage(driver);
        calculatorPage = new CalculatorPage(driver);
    });

    after(async function() {
        await driver.quit();
    });

    it("Open Google Cloud", async function() {
        await indexPage.openHomepage();
        await indexPage.search("Google Cloud Platform Pricing Calculator");

        await searchPage.findResult("Google Cloud Pricing Calculator");

        await calculatorPage.addToEstimate("Compute Engine");
        await calculatorPage.setInstances(4);
        await calculatorPage.setOS("Free: ");
        await calculatorPage.setInstanceType("n1-standard-8");
        await calculatorPage.setGPU("NVIDIA Tesla V100", 1);
        await calculatorPage.setSSD("2x375");
        await calculatorPage.setRegion("europe-west3");

        await driver.wait(async () => await calculatorPage.isPriceCalculated(), 10000, 'Price not calculated after 10s');

        const price = await calculatorPage.getPrice();
        console.log(price);
    });
});
