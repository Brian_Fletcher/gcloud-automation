const { By, until } = require('selenium-webdriver');

class SearchResultsPage {
    constructor(driver) {
        this.driver = driver;
    }

    async findResult(query) {
        try {
            const button = await this.driver.wait(until.elementLocated(By.linkText(query)), 10000);

            await this.driver.wait(until.elementIsVisible(button), 10000);

            await button.click();
        } catch (error) {
            console.error("Error occurred while locating or clicking the element:", error);
        }
    }


    async isResultVisible(query) {
        const element = await this.driver.findElement(By.css(query));
        return await element.isDisplayed();
    }
}

module.exports = SearchResultsPage;
