const { Builder, By, Key, until } = require('selenium-webdriver');

class IndexPage {

    constructor(driver) {
        this.driver = driver;
    }
    async openHomepage() {
        await this.driver.get("https://cloud.google.com/");
    }

    async search(query) {
        const searchIcon = await this.driver.findElement(By.css(".YSM5S"));
        await searchIcon.click();

        const searchInput = await this.driver.findElement(By.css("#i5"));
        await searchInput.sendKeys(query);

        await searchInput.sendKeys(Key.ENTER);
    }
}

module.exports = IndexPage;
