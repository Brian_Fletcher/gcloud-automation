const { By, until } = require('selenium-webdriver');

class CalculatorPage {
    constructor(driver) {
        this.driver = driver;
    }

    async addToEstimate(query) {
        try {
            const addButton = await this.driver.wait(until.elementLocated(By.css(".UywwFc-vQzf8d")), 10000);
            await this.driver.wait(until.elementIsVisible(addButton), 10000);

            await addButton.click();

            const queryElement = await this.driver.wait(until.elementLocated(By.xpath(`//div//h2[contains(text(), '${query}')]`)), 10000);
            await this.driver.wait(until.elementIsVisible(queryElement), 10000);

            await queryElement.click();

            console.log('Elements clicked successfully!');
        } catch (error) {
            console.error('Error occurred while clicking the elements:', error);
        }
    }

    async setInstances(count) {
        try {
            const instancesInput = await this.driver.wait(until.elementLocated(By.css("#c9")), 10000);

            await this.driver.wait(until.elementIsVisible(instancesInput), 10000);

            await this.driver.wait(until.elementIsEnabled(instancesInput), 10000);

            await instancesInput.clear();
            await instancesInput.sendKeys(count);

            console.log('Instances set successfully!');
        } catch (error) {
            console.error('Error occurred while setting instances:', error);
        }
    }

    async setOS(os) {
        try {
            const osDropdown = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[1]/div/div[2]/div[3]/div[7]/div/div[1]/div/div/div/div[1]")), 10000);

            await this.driver.wait(until.elementIsVisible(osDropdown), 10000);
            await this.driver.wait(until.elementIsEnabled(osDropdown), 10000);

            await osDropdown.click();

            const osOption = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@class, 'VfPpkd-rymPhb')]//li//span[contains(text(), '${os}')]`)), 10000);

            await this.driver.wait(until.elementIsVisible(osOption), 10000);
            await this.driver.wait(until.elementIsEnabled(osOption), 10000);

            await osOption.click();

            console.log('OS set successfully!');
        } catch (error) {
            console.error('Error occurred while setting OS:', error);
        }
    }

    async setInstanceType(query) {
        try {
            const instanceTypeDropdown = await this.driver.wait(until.elementLocated(By.css("#c31")), 10000);

            await this.driver.wait(until.elementIsVisible(instanceTypeDropdown), 10000);
            await this.driver.wait(until.elementIsEnabled(instanceTypeDropdown), 10000);

            await instanceTypeDropdown.click();

            const queryElement = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@class, 'VfPpkd-rymPhb')]//li//span[contains(text(), '${query}')]`)), 10000);

            await this.driver.wait(until.elementIsVisible(queryElement), 10000);
            await this.driver.wait(until.elementIsEnabled(queryElement), 10000);

            await queryElement.click();

            console.log('Instance type set successfully!');
        } catch (error) {
            console.error('Error occurred while setting instance type:', error);
        }
    }

    async setGPU(model, number) {
        try {
            const gpuButton = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[1]/div/div[2]/div[3]/div[21]/div/div/div[1]/div/div/span/div/button/div/span[1]")), 10000);

            await this.driver.wait(until.elementIsVisible(gpuButton), 10000);
            await this.driver.wait(until.elementIsEnabled(gpuButton), 10000);

            await gpuButton.click();

            const modelOption = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@aria-label, 'GPU Model')]//span[contains(text(), '${model}')]`)), 10000);

            await this.driver.wait(until.elementIsVisible(modelOption), 10000);
            await this.driver.wait(until.elementIsEnabled(modelOption), 10000);

            await modelOption.click();

            const numberOption = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@aria-label, 'Number of GPUs')]//span[contains(text(), '${number}')]`)), 10000);

            await this.driver.wait(until.elementIsVisible(numberOption), 10000);
            await this.driver.wait(until.elementIsEnabled(numberOption), 10000);

            await numberOption.click();

            console.log('GPU set successfully!');
        } catch (error) {
            console.error('Error occurred while setting GPU:', error);
        }
    }

    async setSSD(type) {
        const ssdDropdown = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[1]/div/div[2]/div[3]/div[27]/div/div[1]/div/div/div/div[1]")), 10000);
        await this.driver.wait(until.elementIsVisible(ssdDropdown), 10000);
        await ssdDropdown.click();

        const typeOption = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@aria-label, 'Local SSD')]//span[contains(text(), '${type}')]`)), 10000);
        await this.driver.wait(until.elementIsVisible(typeOption), 10000);
        await typeOption.click();
    }

    async setRegion(region) {
        const regionDropdown = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[1]/div/div[2]/div[3]/div[29]/div/div[1]/div/div/div/div[1]")), 10000);
        await this.driver.wait(until.elementIsVisible(regionDropdown), 10000);
        await regionDropdown.click();

        const regionOption = await this.driver.wait(until.elementLocated(By.xpath(`//ul[contains(@aria-label, 'Region')]//span[contains(text(), '${region}')]`)), 10000);
        await this.driver.wait(until.elementIsVisible(regionOption), 10000);
        await regionOption.click();
    }

    async isPriceCalculated() {
        const priceLabel = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[2]/div[1]/div/div[4]/div[1]/div[2]/label")), 10000);
        return await this.driver.wait(until.elementIsVisible(priceLabel), 10000);
    }

    async getPrice() {
        const priceLabel = await this.driver.wait(until.elementLocated(By.xpath("//*[@id=\"ow4\"]/div[2]/div[1]/div/div[4]/div[1]/div[2]/label")), 10000);
        await this.driver.wait(until.elementIsVisible(priceLabel), 10000);
        return await priceLabel.getText();
    }
}

module.exports = CalculatorPage;
